### Sinatra
- Creamos una nueva carpeta y nos situamos en ella.
- Dentro de la carpeta, creamos un archivo llamado:

`server.rb`

Instalamos Sinatra 

 `   gem install sinatra.`
### Nuestro Modelo
Lo que haremos es que en lugar de usar una base de datos, solo usaremos una matriz de hashes. Así que vamos a crear un conjunto de datos inicial que represente las publicaciones del blog.

`server.rb`

```ruby
require 'sinatra'

posts = [{title: "First Post", body: "content of first post"}]
```
### La ruta del índice
La ruta de índice muestra todos los elementos del modelo. Por lo tanto, esta ruta debería devolver todas las publicaciones cuando se envía una solicitud get a "/posts".

`server.rb`

```ruby
require 'sinatra'

##modelo/dataset

posts = [{title: "First Post", body: "content of first post"}]


## Index route
get '/posts' do
    # Return all the posts as JSON
    return posts.to_json
end
```
- Ejecutar el servidor ruby server.rb
- Vaya a localhost:4567/posts en su navegador.

### La ruta de presentacion
La ruta de presentación le permite ver uno de los elementos basados en un ID, ya que estamos usando una matriz en lugar de una base de datos, pretenderemos que el índice de la matriz es el número de identificación.

```ruby
require 'sinatra'

##modelo/dataset

posts = [{title: "First Post", body: "content of first post"}]


## Index route
get '/posts' do
    #Regresa todos los posts como JSON
    return posts.to_json
end

##mostrar ruta
get '/posts/:id' do
# devuelve una publicación en particular como json según el parámetro de identificación de la URL
    # Los parámetros siempre llegan a una cadena, por lo que los convertimos a un número entero
    id = params["id"].to_i
    return posts[id].to_json
end
```
- Ejecute el servidor y pruebe /posts/0

### La ruta Crear
La ruta de creación normalmente recibe información en el cuerpo de la solicitud y crea una nueva entrada de datos. Esta sería una solicitud posterior a ./posts

Para obtener el cuerpo de la solicitud, definimos un método personalizado anteriormente en el archivo, asegúrese de agregarlo.
```ruby
require 'sinatra'


##modelo/dataset

posts = [{title: "First Post", body: "content of first post"}]

##metodo personalizado para obtener solicitud de body
def getBody (req)
   #se hace rewind al body en caso de que ya haya sido leido
    req.body.rewind
   #se hace parse al body
    return JSON.parse(req.body.read)
end


## Index route
get '/posts' do
    #Regresa todos los posts como JSON
    return posts.to_json
end

# devuelve una publicación en particular como json según el parámetro de identificación de la URL
    # Los parámetros siempre llegan a una cadena, por lo que los convertimos a un número entero
    id = params["id"].to_i
    return posts[id].to_json
end

##crear la ruta
post '/posts' do
    #Se pasa la solicitud a la funcion getbody personalizado
    body = getBody(request)
    #Se crea el nuevo post
    new_post = {title: body["title"], body: body["body"]}
    #Se hace push al post en el arreglo
    posts.push(new_post)
    #se regresa el post nuevo
    return new_post.to_json
end
```

- Usando una herramienta como Postman para hacer una solicitud de publicación a /posts, asegúrese de incluir un cuerpo JSON adecuado.

```ruby
{
    "title": "Another Post",
    "body":"content in the new post"
}
```

### La ruta de actualización
La ruta de actualización debe recibir el identificador del elemento para actualizar en la dirección URL y, a continuación, actualizarlo utilizando los datos pasados a través del cuerpo de la solicitud. Esto se hace normalmente a través de una solicitud Put y/o Patch a /posts/:id.

```ruby
require 'sinatra'

## Model/Dataset

posts = [{title: "First Post", body: "content of first post"}]

## Custom Method for Getting Request body
def getBody (req)
    ## Rewind the body in case it has already been read
    req.body.rewind
    ## parse the body
    return JSON.parse(req.body.read)
end


## Index route
get '/posts' do
    # Return all the posts as JSON
    return posts.to_json
end

## Show Route
get '/posts/:id' do
    # return a particular post as json based on the id param from the url
    # Params always come to a string so we convert to an integer
    id = params["id"].to_i
    return posts[id].to_json
end

## Create Route
post '/posts' do
    # Pass the request into the custom getBody function
    body = getBody(request)
    # create the new post
    new_post = {title: body["title"], body: body["body"]}
    # push the new post into the array
    posts.push(new_post)
    # return the new post
    return new_post.to_json
end

## Update Route
put '/posts/:id' do
    # get the id from params
    id = params["id"].to_i
    # get the request body
    body = getBody(request)
    #update the item in question
    posts[id][:title] = body["title"]
    posts[id][:body] = body["body"]
    #return the updated post
    return posts[id].to_json
end
```

- Realizamos una solicitud put en /posts/0 e incluya un cuerpo JSON adecuado para probar.

### La ruta de la destrucción
La ruta de destrucción toma un identificador de un elemento que se eliminará en la url y lo elimina. Esto se hace haciendo una solicitud de eliminación a /posts/:id

```ruby
require 'sinatra'

##modelo/dataset

posts = [{title: "First Post", body: "content of first post"}]

##metodo personalizado para obtener solicitud de body
def getBody (req)
    #se hace rewind al body en caso de que ya haya sido leido
    req.body.rewind
    #se hace parse al body
    return JSON.parse(req.body.read)
end


## Index route
get '/posts' do
    #Regresa todos los posts como JSON
    return posts.to_json
end

##mostrar ruta
get '/posts/:id' do
    #regresa un post particular basado en un json
    id = params["id"].to_i
    return posts[id].to_json
end

##crear la ruta
post '/posts' do
    #Se pasa la solicitud a la funcion getbody personalizado
    body = getBody(request)
    #Se crea el nuevo post
    new_post = {title: body["title"], body: body["body"]}
    #Se hace push al post en el arreglo
    posts.push(new_post)
    #se regresa el post nuevo
    return new_post.to_json
end

##Ruta update
put '/posts/:id' do
    #Obtener el id de los parametros
    id = params["id"].to_i
    #Obtener la solicitud del body
    body = getBody(request)
    #actualizar el objeto en cuestion
    posts[id][:title] = body["title"]
    posts[id][:body] = body["body"]
   #regresa el post actualizado
    return posts[id].to_json
end


#ruta destroy
delete '/posts/:id' do
    #se obtiene el id desde parametros
    id = params["id"].to_i
    #eliminar el objeto
    post = posts.delete_at(id)
    #regresa el objeto eliminado como json
    return post.to_json
end
```

- Probamos haciendo una solicitud de eliminación a /posts/0