require 'sinatra'

##modelo/dataset

posts = [{title: "First Post", body: "content of first post"}]

##metodo personalizado para obtener solicitud de body
def getBody(req)
    #se hace rewind al body en caso de que ya haya sido leido
    req.body.rewind
    #se hace parse al body
    return JSON.parse(req.body.read)
end 

##index
get '/posts' do
    #Regresa todos los posts como JSON
    return posts.to_json
end

##mostrar ruta
get '/posts/:id' do
    #regresa un posta particular basado en un json
    id = params["id"].to_i
    return posts[id].to_json
end

##crear la ruta
post '/posts' do
    #Se pasa la solicitud a la funcion getbody personalizado
    body = getBody(request)
    #Se crea el nuevo post
    new_post = {title: body["title"], body: body["body"]}
    #Se hace push al post en el arreglo
    posts.push(new_post)
    #se regresa el post nuevo
    return new_post.to_json
end

##Ruta update
put '/posts/:id' do
    #Obtener el id de los parametros
    id = params["id"].to_i
    #Obtener la solicitud del body
    body = getBody(request)
    #actualizar el objeto en cuestion
    posts[id][:title] = body["title"]
    posts[id][:body] = body["body"]
    #regresa el post actualizado
    return posts[id].to_json
end

#ruta destroy
delete 'posts/:id' do
    #se obtiene el id desde parametros
    id = params["id"].to_i
    #eliminar el objeto
    post = posts.delete_at(id)
    #regresa el objeto eliminado como json
    return post.to_json
end